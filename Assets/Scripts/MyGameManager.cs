﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MyGameManager : MonoBehaviour
{
    public static MyGameManager instance;

    [HideInInspector]
    public bool startGame, failed, successed;
    public GameObject successPanel, failPanel, startGamePanel;

    private void Awake()
    {
        Application.targetFrameRate = 60;
        if (instance == null) instance = this;
        //   TinySauce.OnGameStarted(PlayerPrefs.GetInt("CurrentLevel", 1).ToString());
    }

    public void StartGame()
    {
        startGame = true;
        startGamePanel.SetActive(false);
        Car_Movement.instance.state = Car_Movement.State.Moving;
    }

    public void Fail()
    {
        if (!failed)
        {
            failed = true;
            failPanel.SetActive(true);
        }
    }
    public void Success()
    {
        successed = true;
        successPanel.SetActive(true);
        PlayerPrefs.SetInt("CurrentLevel", PlayerPrefs.GetInt("CurrentLevel", 1) + 1);
    }

    public void NextLevel()
    {
        //  TinySauce.OnGameFinished(PlayerPrefs.GetInt("CurrentLevel", 1).ToString(), true, 1);
        //  LoadAsync.GlobalAsync.TriggerManually();
        SceneManager.LoadScene(1);

    }

    public void RestartGame()
    {
        SceneManager.LoadScene(1);
        // Destroy(this);
    }
}
