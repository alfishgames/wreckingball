﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public static CameraFollow instance;
    public enum CamState { WithRotation, NoRotation, NoRotationWithOffset, DontFollow }
    public CamState camState;

    [Range(0.0f, 100.0f)]
    public float smoothness;

    public Transform target;
    private Transform player;
    [HideInInspector]
    public Vector3 offset;

    private void Awake()
    {
        if (instance == null) instance = this;
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    private void FixedUpdate()
    {
        FollowPlayer();
        FollowTarget();
        FollowRotation();

        if (camState == CamState.DontFollow)
        {

        }
    }

    private void Start()
    {
        offset = target.transform.position;
    }

    private void FollowPlayer()
    {
        if (camState == CamState.NoRotationWithOffset)
        {
            Vector3 desiredPosition = player.position + offset;
            transform.position = Vector3.Lerp(transform.position, desiredPosition, Time.deltaTime * smoothness);
        }
    }

    private void FollowTarget()
    {
        if (camState == CamState.NoRotation)
        {
            Vector3 desiredPosition = target.position + offset;
            transform.position = Vector3.Lerp(transform.position, desiredPosition, Time.deltaTime * smoothness);
        }
    }

    private void FollowRotation()
    {
        if (camState == CamState.WithRotation)
        {
              Vector3 desiredPosition = target.position;

            float xCamAngle = transform.rotation.eulerAngles.x;
            float yPlayerAngle = player.rotation.eulerAngles.y;

            transform.position = Vector3.Lerp(transform.position, desiredPosition, Time.deltaTime * smoothness);
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(xCamAngle, yPlayerAngle - 5, 0), Time.deltaTime * 8);
        }
    }
}
