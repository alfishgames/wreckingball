using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Building : MonoBehaviour
{
    public Rigidbody[] rbs;

    private bool canDestroy;
    private float timer;
    public Color[] a;

    private void Start()
    {
        timer = 1f;
        a = new Color[rbs.Length];
    }

    private void Update()
    {
        DestroyParticles();
    }

    public void DemolishThisShit()
    {
        for (int i = 0; i < rbs.Length; i++)
        {
            a[i] = rbs[i].gameObject.GetComponent<MeshRenderer>().material.color;
            rbs[i].isKinematic = false;
            rbs[i].gameObject.GetComponent<MeshRenderer>().material.color = a[i];
            a[i].a = timer;
        }
        canDestroy = true;
    }

    private void DestroyParticles()
    {
        if (canDestroy)
        {
            timer -= Time.deltaTime;

            if (timer < 0)
            {
                for (int i = 0; i < rbs.Length; i++)
                {
                    if (rbs[i] != null) Destroy(rbs[i].gameObject);
                }
            }
        }
    }
}
