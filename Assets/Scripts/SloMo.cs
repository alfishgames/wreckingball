using UnityEngine;
using Obi;

public class SloMo : MonoBehaviour
{
    public static SloMo instance;
    public ObiSolver solver;
    public float slowDownFactor = 0.1f;
    public float slowDownLength = 5f;

    private void Start()
    {
        DoSloMo();
    }
    private void Awake()
    {
        if (instance == null) instance = this;
    }

    private void Update()
    {
        Time.timeScale += (1f / slowDownLength) * Time.unscaledDeltaTime;
        Time.timeScale = Mathf.Clamp(Time.timeScale, 0f, 1f);
    }

    public void DoSloMo()
    {
        Time.timeScale = slowDownFactor;
        Time.fixedDeltaTime = Time.timeScale * 0.04f;
    }
}
