﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Obi;

public class Car_Movement : MonoBehaviour
{
    public static Car_Movement instance;

    public enum State { Idle, Moving, Falling, FallDown }
    public State state;

    [Range(0,10)]
    public float forwardSpeed;
    [Range(0,10)]
    public float turnSpeed;
    public GameObject worldCollider;
    public GameObject smokes;
    public Collider ballCol;
    private Rigidbody myRb;
    private Collider myCol;
    private bool fall;

    private void Awake()
    {
        if (instance == null) instance = this;

        myRb = GetComponent<Rigidbody>();
        myCol = GetComponent<Collider>();
    }
    private void Start()
    {
        Physics.IgnoreCollision(myCol, ballCol);
    }

    private void Update()
    {
        StateManager();
        RotationFunction();
        DriftForce();
    }

    private void FixedUpdate()
    {
        MovementFunction();
    }

    private void StateManager()
    {
        if (state == State.Idle)
        {
            myRb.isKinematic = true;
        }

        if (state == State.Falling)
        {
            myRb.constraints = RigidbodyConstraints.None;
            myRb.useGravity = true;

            myRb.velocity = transform.forward * forwardSpeed * 1;
            StartCoroutine(FallDelay());
        }

        if (state == State.FallDown)
        {
            myRb.constraints = RigidbodyConstraints.None;
            myRb.AddForce(transform.forward * 115);
        }
    }

    private void MovementFunction()
    {
        if (state == State.Moving && state != State.Falling)
        {
            myRb.isKinematic = false;
            if (!fall) transform.position += transform.forward * forwardSpeed * 1.8f * Time.deltaTime;
        }
    }

    private void DriftForce()
    {
        if (state == State.Moving && state != State.Falling)
        {
            if (Input.GetMouseButtonDown(0) && state != State.FallDown)
            {
                myRb.AddForce(Vector3.forward * 6f, ForceMode.Impulse);
                StartCoroutine(Smoker());
            }

            if (Input.GetMouseButtonUp(0) && state != State.FallDown)
            {
                myRb.AddForce(Vector3.right * 6f, ForceMode.Impulse);
                StartCoroutine(Smoker());
            }
        }
    }

    private void RotationFunction()
    {
        if (state == State.Moving && state != State.Falling)
        {
            if (Input.GetMouseButton(0))
            {
                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0, 90, 0), Time.deltaTime * 2.2f * turnSpeed);
            }

            else
            {
                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0, 0, 0), Time.deltaTime * 2.2f * turnSpeed);
            }
        }
    }

    private IEnumerator Smoker()
    {
        smokes.SetActive(true);
        yield return new WaitForSeconds(0.6f);
        smokes.SetActive(false);
        myRb.constraints = RigidbodyConstraints.FreezeAll;
        yield return new WaitForSeconds(1.6f);
        myRb.constraints = RigidbodyConstraints.None;
        myRb.constraints = RigidbodyConstraints.FreezePositionY;
    }

    private IEnumerator FailDelay()
    {
        yield return new WaitForSeconds(1.2f);
        MyGameManager.instance.Fail();
    } 
    private IEnumerator SuccessDelay()
    {
        yield return new WaitForSeconds(1.2f);
        MyGameManager.instance.Success();
    }  
    private IEnumerator FallDelay()
    {
        myRb.constraints = RigidbodyConstraints.None;
        yield return new WaitForSeconds(0.1f);
        state = State.FallDown;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Fall_Trigger"))
        {

            myRb.constraints = RigidbodyConstraints.None;
            myCol.isTrigger = false;
            worldCollider.SetActive(false);
            state = State.Falling;
            ballCol.gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
            Camera.main.GetComponent<CameraFollow>().camState = CameraFollow.CamState.DontFollow;
            myRb.constraints = RigidbodyConstraints.None;
            GameObject obiRope = FindObjectOfType<ObiRope>().gameObject;

            obiRope.SetActive(false);
            StartCoroutine(FailDelay());
        }

        if (other.gameObject.CompareTag("Finish_Trigger"))
        {
            GameObject finGo = GameObject.FindGameObjectWithTag("Finish_Obstacle");
            GameObject ballGo = FindObjectOfType<Ball>().gameObject;
          //  GameObject obiRope = FindObjectOfType<ObiRope>().gameObject;
            Rigidbody ballRb = ballGo.GetComponent<Rigidbody>();
            CameraFollow cameraFollow = Camera.main.GetComponent<CameraFollow>();

            FindObjectOfType<ObiRope>().gameObject.SetActive(false);

            cameraFollow.camState = CameraFollow.CamState.NoRotation;
          //  cameraFollow.offset = new Vector3 ()
            cameraFollow.target = ballGo.transform;

            ballRb.constraints = RigidbodyConstraints.None;
            ballRb.AddForce(((-ballGo.transform.position + finGo.transform.position) * 350 + Vector3.up * 20) * 1);
            StartCoroutine(SuccessDelay());
            state = State.Idle;
        }
    }
}
