using UnityEngine;

public class Ball : MonoBehaviour
{
    public static Ball instance;
    public GameObject wreckSmoke;

    private void Awake()
    {
        if (instance == null) instance = this;
        wreckSmoke.transform.localScale = Vector3.one * 3;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Obstacle") || other.gameObject.CompareTag("Finish_Obstacle"))
        {
            SloMo.instance.DoSloMo();
            other.gameObject.GetComponent<Building>().DemolishThisShit();
            Instantiate(wreckSmoke, transform.position, Quaternion.identity);
            
        }
    }
}
